package com.beawergames.game.util

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface JumperServerClient {

    @FormUrlEncoded
    @POST("JumperGame/splash.php")
    fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Call<JumperSplashResponse>

    companion object {
        fun create() : JumperServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(JumperServerClient::class.java)
        }
    }

}