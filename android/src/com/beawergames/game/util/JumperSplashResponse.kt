package com.beawergames.game.util

import androidx.annotation.Keep

@Keep
class JumperSplashResponse(val url : String)