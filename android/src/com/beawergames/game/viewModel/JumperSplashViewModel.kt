package com.beawergames.game.viewModel

import android.os.Build
import android.view.View
import android.widget.ImageView
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import androidx.databinding.library.baseAdapters.BR
import com.beawergames.game.util.JumperServerClient
import com.beawergames.game.util.JumperSplashResponse
import com.squareup.picasso.Picasso
import im.delight.android.webview.AdvancedWebView
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


@BindingAdapter("imageData")
fun setImageFromUrl(v: ImageView, url: String?) {
    Picasso.get().load(url).into(v)
}

@BindingAdapter("url")
fun setUrl(v: AdvancedWebView, url: String) {
    v.loadUrl(url)
}

class JumperSplashViewModel(simLanguage: String): BaseObservable() {
    private var requestInProcess = false

    private val mutableFlow = MutableSharedFlow<Boolean>(1, 1, BufferOverflow.DROP_OLDEST)
    val outputFlow = mutableFlow.asSharedFlow()

    @Bindable
    var url  = ""
    @Bindable
    var logoUrl = "http://195.201.125.8/JumperGame/logo.png"
    @Bindable
    var webViewVisibility = View.VISIBLE
    @Bindable
    var logoVisibility = View.GONE

    init {
        showLogo()
        JumperServerClient.create().getSplash(Locale.getDefault().language, simLanguage, Build.MODEL, TimeZone.getDefault().displayName.replace("GMT", ""))
            .enqueue(object : Callback<JumperSplashResponse> {
                override fun onResponse(
                    call: Call<JumperSplashResponse>,
                    response: Response<JumperSplashResponse>
                ) {
                    if (requestInProcess) {
                    requestInProcess = false
                    hideLogo()
                    }
                    response.body()?.let {
                        if(it.url != "no") {
                            url = "https://${it.url}"
                            notifyPropertyChanged(BR.url)
                        } else switchToMain()
                    } ?: switchToMain()
                }

                override fun onFailure(call: Call<JumperSplashResponse>, t: Throwable) {
                    switchToMain()
                }
            })
    }

    private fun showLogo() {
        if(requestInProcess) {
            webViewVisibility = View.GONE
            notifyPropertyChanged(BR.webViewVisibility)
            logoVisibility = View.VISIBLE
            notifyPropertyChanged(BR.logoVisibility)
        }
    }

    private fun switchToMain() {
        mutableFlow.tryEmit(true)
    }

    fun hideLogo() {
        webViewVisibility = View.VISIBLE
        notifyPropertyChanged(BR.webViewVisibility)
        logoVisibility = View.GONE
        notifyPropertyChanged(BR.logoVisibility)
    }
}