package com.beawergames.game.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json

class Records {
    companion object {
        fun saveRecord(record: Int) {
            val prefs = Gdx.app.getPreferences("Records")
            val j = Json()
            val records = j.fromJson(Array<Int>::class.java, prefs.getString("records", "[]"))
            prefs.putString("records", j.toJson(records.plusElement(record)))
            prefs.flush()
        }

        fun getRecords(): Array<Int> {
            val prefs = Gdx.app.getPreferences("Records")
            val r = prefs.getString("records", "[]")
            return Json().fromJson(Array<Int>::class.java, r)
        }
    }
}