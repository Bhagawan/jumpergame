package com.beawergames.game

import com.badlogic.gdx.Game
import com.beawergames.game.assets.Assets
import com.beawergames.game.screen.MenuScreen

class JumperGame: Game() {

	companion object {
		const val PERFECT_DT: Float = (16.67 * 2).toFloat()
	}

	override fun create() {
		Assets.create()
		setScreen(MenuScreen(this))
	}

	override fun dispose() {
		super.dispose()
		Assets.dispose()
	}
}
