package com.beawergames.game.systems

import com.badlogic.ashley.core.*
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.math.Rectangle
import com.beawergames.game.assets.Assets
import com.beawergames.game.components.*

class BonusInteractionSystem(priority: Int): IteratingSystem(Family.all(BonusComponent::class.java).get(), priority),
    EntityListener {
    private var hero: Entity? = null
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val bM = ComponentMapper.getFor(BonusComponent::class.java)
    private val pM = ComponentMapper.getFor(PLayerComponent::class.java)
    private var currHeroPos: TransformComponent? = null
    private var pC: PLayerComponent? = null

    private var currBonus: Int? = null

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        engine?.addEntityListener(this)
        hero = engine?.getEntitiesFor(Family.all(PLayerComponent::class.java).get())?.firstOrNull()
    }

    override fun update(deltaTime: Float) {
        hero?.let {
            if(currHeroPos == null) currHeroPos = tM.get(it)
            if(pC == null) pC = pM.get(it)
            if(currHeroPos != null && pC != null) {
                currBonus?.let { bonus ->
                    when(bonus) {
                        BonusComponent.APPLE -> {
                            pC?.let { pC->
                                pC.currentVelocity -= 10 * deltaTime
                                if(pC.currentVelocity <= 0) currBonus = null
                            }
                        }
                        BonusComponent.PUSH -> {
                            pC?.let { pC->
                                pC.currentVelocity -= 15 * deltaTime
                                if(pC.currentVelocity <= 0) currBonus = null
                            }
                        }
                        BonusComponent.ARROW -> {
                            pC?.let { pC->
                                pC.currentVelocity -= 5 * deltaTime
                                if(pC.currentVelocity <= 0) currBonus = null
                            }
                        }
                        else -> {}
                    }
                }
                super.update(deltaTime)
            }
        }
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val bC = bM.get(entity)
        val tC = tM.get(entity)
        if(bC != null && tC != null && currHeroPos != null && pC != null) {
            if(Rectangle(tC.pos.x, tC.pos.y, tC.width, tC.height).overlaps(Rectangle(currHeroPos!!.pos.x, currHeroPos!!.pos.y, currHeroPos!!.width, currHeroPos!!.height))) {
                pC?.state = PLayerComponent.STATE_JUMP_UP
                when(bC.type) {
                    BonusComponent.APPLE -> {
                        currBonus = BonusComponent.APPLE
                        engine.removeEntity(entity)
                        pC?.let {
                            it.currentVelocity += 15
                        }
                    }
                    BonusComponent.PUSH -> {
                        currBonus = BonusComponent.PUSH
                        entity?.add(AnimationComponent(Assets.animPush))
                        pC?.let {
                            it.currentVelocity += 30
                        }
                    }
                    BonusComponent.ARROW -> {
                        currBonus = BonusComponent.ARROW
                        engine.removeEntity(entity)
                        pC?.let {
                            it.currentVelocity += 35
                        }
                    }
                }
                }
        }
    }

    override fun entityAdded(entity: Entity?) {
        entity?.let {
            if (it.getComponent(PLayerComponent::class.java) != null) hero = it
        }
    }

    override fun entityRemoved(entity: Entity?) {
        if(entity == hero){
            hero = null
            currHeroPos = null
        }
    }
}