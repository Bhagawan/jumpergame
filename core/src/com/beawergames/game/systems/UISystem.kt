package com.beawergames.game.systems

import com.badlogic.ashley.core.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.beawergames.game.JumperGame
import com.beawergames.game.assets.Assets
import com.beawergames.game.components.PLayerComponent

class UISystem(priority: Int): EntitySystem(priority), EntityListener {
    private var hero: Entity? = null
    private val stage = object: Stage() {
        override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {

            return super.touchDown(screenX, screenY, pointer, button)
        }
    }
    private val backgroundTable = Table()
    private val uiTable = Table()
    private val scoreLabel = Label("0", Label.LabelStyle(Assets.russianFont, Color.BLACK))
    private val menuTable = Table()
    private var mInterface: UIInterface? = null

    init {
        backgroundTable.setFillParent(true)
        stage.addActor(backgroundTable)
        uiTable.setFillParent(true)
        stage.addActor(uiTable)
        //uiTable.debug = true

        val image = Image(Assets.halfTransparent)
        backgroundTable.top().add(image).expandX().height(50.0f).fill()

        val pauseBtn = Button(TextureRegionDrawable(Assets.btnPause))
        pauseBtn.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(!menuTable.isVisible) {
                    menuTable.isVisible = true
                    mInterface?.pause(true)
                } else {
                    mInterface?.pause(false)
                    menuTable.isVisible = false
                }
            }
        })
        uiTable.top().add(pauseBtn).top().left().padLeft(5.0f).padTop(5.0f).width(40.0f).height(40.0f)
        uiTable.center().top().add(scoreLabel).center().expandX().padTop(5.0f)

        menuTable.name = "pauseMenu"
        menuTable.background = TextureRegionDrawable(Assets.greyColor)

        val menuBtnBack = TextureRegionDrawable(Assets.buttonDown).tint(Color.GRAY)

        val resumeBtn = TextButton("продолжить", TextButton.TextButtonStyle(menuBtnBack, null, null, Assets.russianFont))
        resumeBtn.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                mInterface?.pause(false)
                menuTable.isVisible = false
            }
        })
        menuTable.center().add(resumeBtn).expandX().height(50.0f).width(Gdx.graphics.width / 1.5f).padBottom(10.0f)
        menuTable.row()
        val exitBtn = TextButton("выход", TextButton.TextButtonStyle(menuBtnBack, null, null, Assets.russianFont))
        exitBtn.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                mInterface?.exit()
            }
        })
        menuTable.add(exitBtn).expandX().height(50.0f).width(Gdx.graphics.width / 1.5f)
        uiTable.row()
        uiTable.add(menuTable).expand().center().colspan(2).width( Gdx.graphics.width / 1.2f).height(130.0f)
        menuTable.isVisible = false

    }

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        engine?.addEntityListener(this)
        hero = engine?.getEntitiesFor(Family.all(PLayerComponent::class.java).get())?.firstOrNull()
    }

    override fun update(deltaTime: Float) {
        hero?.let {
            val score = it.getComponent(PLayerComponent::class.java)?.height ?: 0
            scoreLabel.setText((score / 10).toString())
        }

        stage.act(Gdx.graphics.deltaTime.coerceAtMost(JumperGame.PERFECT_DT))
        stage.draw()
    }

    override fun entityAdded(entity: Entity?) {
        entity?.let {
            if (it.getComponent(PLayerComponent::class.java) != null) hero = it
        }
    }

    override fun entityRemoved(entity: Entity?) {
        if(entity == hero) hero = null
    }

    fun setInterface(i: UIInterface) {
        mInterface = i
    }

    fun getStage(): Stage {
        return stage
    }

    fun showEndScreen(height: Int) {
        val endScreen = Dialog("", Window.WindowStyle(Assets.russianFont, Color.WHITE, TextureRegionDrawable(Assets.greyColor)))
        endScreen.pad(20.0f)

        val dBack = TextureRegionDrawable(Assets.buttonDown).tint(Color.GRAY)
        val againBtn = TextButton("заново", TextButton.TextButtonStyle(dBack, null, null, Assets.russianFont))
        againBtn.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                mInterface?.newGame()
            }
        })
        val exitBtn = TextButton("выход", TextButton.TextButtonStyle(dBack, null, null, Assets.russianFont))
        exitBtn.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                mInterface?.exit()
            }
        })
        endScreen.contentTable.add(Label("Конец", Label.LabelStyle(Assets.russianFont, Color.WHITE)))
        endScreen.contentTable.row()
        endScreen.contentTable.add(Label("Ваша высота: $height", Label.LabelStyle(Assets.russianFont, Color.WHITE)))
        endScreen.button(againBtn)
        endScreen.button(exitBtn)
        endScreen.show(stage)
    }

    interface UIInterface {
        fun pause(pause: Boolean)
        fun exit()
        fun newGame()
    }
}