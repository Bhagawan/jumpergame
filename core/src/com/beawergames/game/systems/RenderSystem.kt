package com.beawergames.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.beawergames.game.components.DrawComponent
import com.beawergames.game.components.TransformComponent
import kotlin.math.sign
import com.badlogic.gdx.utils.Array
import com.beawergames.game.components.AnimationComponent


class RenderSystem(private val bath: SpriteBatch, priority: Int):
    IteratingSystem(Family.all(DrawComponent::class.java, TransformComponent::class.java).get(), priority) {

    private val dM = ComponentMapper.getFor(DrawComponent::class.java)
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val aM = ComponentMapper.getFor(AnimationComponent::class.java)

    private val queue = Array<Entity>()

    private val comparator = Comparator<Entity> { p0, p1 -> (tM.get(p0).pos.z - tM.get(p1).pos.z).sign.toInt() }

    override fun update(deltaTime: Float) {
        super.update(deltaTime)

        queue.sort(comparator)

        var dC: DrawComponent?
        var aC: AnimationComponent?
        var tC: TransformComponent?

        bath.begin()
        for(entity in queue) {
            dC = dM.get(entity)
            tC = tM.get(entity)
            aC = aM.get(entity)
            if(aC != null) {
                bath.draw(aC.animation.getKeyFrame(aC.state), tC.pos.x, tC.pos.y, tC.width * 0.5f, tC.height * 0.5f,
                    tC.width , tC.height, tC.scale, tC.scale, tC.angle)
                aC.state += deltaTime
            } else bath.draw(dC.textureRegion, tC.pos.x, tC.pos.y, tC.width * 0.5f, tC.height * 0.5f,
                tC.width , tC.height, tC.scale, tC.scale, tC.angle)
        }
        bath.end()

        queue.clear()
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        entity?.let { queue.add(it) }
    }
}