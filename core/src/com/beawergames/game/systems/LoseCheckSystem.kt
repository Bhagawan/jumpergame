package com.beawergames.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.beawergames.game.components.PLayerComponent
import com.beawergames.game.components.TransformComponent
import com.beawergames.game.util.Records

class LoseCheckSystem(priority: Int):IteratingSystem(Family.all(PLayerComponent::class.java).get(), priority) {
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val pM = ComponentMapper.getFor(PLayerComponent::class.java)
    private var mInterface: LoseInterface? = null

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val tC = tM.get(entity)
        val pC = pM.get(entity)

        if(tC != null){
            if(tC.pos.y < -100) {
                Records.saveRecord(pC.height / 10)
                mInterface?.onLost(pC.height / 10)
                engine.removeEntity(entity)
            }
        }
    }

    fun setInterface( i: LoseInterface) {
        mInterface = i
    }

    interface LoseInterface {
        fun onLost(height: Int)
    }
}