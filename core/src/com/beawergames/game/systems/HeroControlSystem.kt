package com.beawergames.game.systems

import com.badlogic.ashley.core.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Rectangle
import com.beawergames.game.assets.Assets
import com.beawergames.game.components.*
import com.beawergames.game.screen.GameScreen

class HeroControlSystem(priority: Int, private val gameState: GameScreen.GameState): EntitySystem(priority), EntityListener {
    private var hero: Entity? = null
    private val platforms = ArrayList<Entity>()
    private var horizontalDelta = 0.0f

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        engine?.addEntityListener(this)
        hero = engine?.getEntitiesFor(Family.all(PLayerComponent::class.java).get())?.firstOrNull()
        val p  = engine?.getEntitiesFor(Family.all(PlatformComponent::class.java).get())
        p?.let {
            for(platform in p) platforms.add(platform)
        }
    }

    override fun update(deltaTime: Float) {
        if(gameState.getGAmeState() != GameScreen.GAME_PAUSED) {
            hero?.let {
                val tC = it.getComponent(TransformComponent::class.java)
                val pC = it.getComponent(PLayerComponent::class.java)
                val aC = it.getComponent(AnimationComponent::class.java)
                val dC = it.getComponent(DrawComponent::class.java)
                when(pC.state) {
                    PLayerComponent.STATE_IDLE -> dC.textureRegion = Assets.heroDefault
                    PLayerComponent.STATE_JUMP_UP -> {
                        tC.pos.x += horizontalDelta
                        if(tC.pos.x > Gdx.graphics.width) tC.pos.x -= Gdx.graphics.width
                        if(tC.pos.x < 0) tC.pos.x += Gdx.graphics.width
                        if (aC == null) it.add(AnimationComponent(Assets.animHeroUp))
                        else{
                            if(aC.animation != Assets.animHeroUp) it.add(AnimationComponent(Assets.animHeroUp))
                            if(pC.currentVelocity <= 0) {
                                it.remove(AnimationComponent::class.java)
                                pC.state = PLayerComponent.STATE_JUMP_MIDDLE
                                dC.textureRegion = Assets.heroJumping
                            }
                            pC.currentVelocity -= 10 * deltaTime // 10 m/s gravitation
                            tC.pos.y += pC.currentVelocity
                        }
                    }
                    PLayerComponent.STATE_JUMP_MIDDLE -> {
                        tC.pos.x += horizontalDelta
                        if(tC.pos.x > Gdx.graphics.width) tC.pos.x -= Gdx.graphics.width
                        if(tC.pos.x < 0) tC.pos.x += Gdx.graphics.width
                        if(onPlatform(tC.pos.x, tC.pos.y)) {
                            pC.state = PLayerComponent.STATE_JUMP_DOWN
                            it.add(AnimationComponent(Assets.animHeroDown))
                            pC.currentVelocity = 0.0f
                        } else {
                            pC.currentVelocity -= 10 * deltaTime
                            tC.pos.y += pC.currentVelocity
                        }
                    }
                    PLayerComponent.STATE_JUMP_DOWN -> {
                        if (aC == null) it.add(AnimationComponent(Assets.animHeroDown))
                        else{
                            if(aC.animation.animationDuration < aC.state) {
                                it.remove(AnimationComponent::class.java)
                                pC.state = PLayerComponent.STATE_JUMP_UP
                                it.add(AnimationComponent(Assets.animHeroUp))
                                pC.currentVelocity = pC.jumpMaxVelocity
                            }
                            true
                        }
                    }
                    else -> false
                }
            }
        }
    }

    override fun entityAdded(entity: Entity?) {
        entity?.let {
            if (it.getComponent(PLayerComponent::class.java) != null) hero = it
            if(it.getComponent(PlatformComponent::class.java) != null && !platforms.contains(it)) platforms.add(it)
        }
    }

    override fun entityRemoved(entity: Entity?) {
        if(entity == hero) hero = null
        if(platforms.contains(entity)) platforms.remove(entity)
    }

    fun move(left: Boolean) {
        horizontalDelta = if (left) -10.0f else 10.0f
    }

    fun stopMove() {
        horizontalDelta = 0.0f
    }

    private fun onPlatform(x: Float, y: Float) : Boolean {
        var tC: TransformComponent
        var broken : Entity? = null
        for(p in platforms) {
            tC = p.getComponent(TransformComponent::class.java)
            if(Rectangle(tC.pos.x, tC.pos.y, tC.width, tC.height).overlaps(Rectangle(x, y, 75.0f, 5.0f))) {
                val pC = p.getComponent(PlatformComponent::class.java)
                if(pC != null) {
                    if(!pC.real) broken = p
                    else return true
                } else return true
            }
        }
        broken?.let { engine.removeEntity(it) }
        return false
    }
}