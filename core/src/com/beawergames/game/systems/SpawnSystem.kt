package com.beawergames.game.systems

import com.badlogic.ashley.core.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.beawergames.game.assets.Assets
import com.beawergames.game.components.*
import java.lang.Integer.min
import kotlin.random.Random

class SpawnSystem(priority: Int): EntitySystem(priority), EntityListener {
    private var hero: Entity? = null

    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val platforms = ArrayList<Entity>()
    private val maxHeight = Gdx.graphics.height * 2

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        engine?.addEntityListener(this)
        hero = engine?.getEntitiesFor(Family.all(PLayerComponent::class.java).get())?.firstOrNull()
        init()
    }

    override fun update(deltaTime: Float) {
        var bonus = true
        val pC = hero?.getComponent(PLayerComponent::class.java)
        pC?.let { bonus = it.currentVelocity < 15 }
        val height = hero?.getComponent(PLayerComponent::class.java)?.height ?: 0
        val maxDistance = min(300 + height / 2000 , 600)

        val min = min(maxDistance - 1, height / 500)

        val h = getHighestPlatform()
        val r = Random.nextInt(min, maxDistance)
        if(maxHeight - h > 50) { //  random number
            createPlatformAt(Random.nextInt(0, Gdx.graphics.width - 100).toFloat(), h + 60 + r, true, height.toFloat(), bonus)

            if(Random.nextInt(100) > 90 && r > 90)
                createPlatformAt(Random.nextInt(0, Gdx.graphics.width - 100).toFloat(), h + Random.nextInt(h.toInt() + 60, h.toInt() + r - 30) , false, height.toFloat(), bonus)
        }
    }

    private fun init() {
        val hero = Entity()
        hero.add(TransformComponent(Gdx.graphics.width / 2,Gdx.graphics.height / 2, 75, 75))
        hero.add(DrawComponent(TextureRegion(Assets.heroDefault)))
        hero.add(PLayerComponent())
        engine.addEntity(hero)

        createPlatformAt(Gdx.graphics.width / 2.0f,Gdx.graphics.height / 2 - 100.0f, true, 0.0f, false)
    }

    override fun entityAdded(entity: Entity?) {
        entity?.let {
            if(it.getComponent(PLayerComponent::class.java) != null) hero = it
            if(it.getComponent(PlatformComponent::class.java) != null && !platforms.contains(it)) platforms.add(it)
        }
    }

    override fun entityRemoved(entity: Entity?) {
        if(entity == hero) hero = null
        if(entity?.getComponent(PlatformComponent::class.java) != null && !platforms.contains(entity)) platforms.add(entity)
    }

    private fun getHighestPlatform() : Float {
        var out = 0.0f
        for(e in platforms) {
            if(e.getComponent(PlatformComponent::class.java).real) {
                val h = tM.get(e).pos.y
                if(h > out) out = h
            }
        }
        return out
    }

    private fun createPlatformAt(x: Float, y: Float, real: Boolean, height: Float, createBonus: Boolean) {
        val platform = Entity()
        var bonus :Entity? = null
        platform.add(PlatformComponent(real))
        platform.add(TransformComponent(x, y, 100.0f, 30.0f))
        platform.add(DrawComponent(if(real) Assets.platformReal else Assets.platformBroken))
        if(Random.nextInt(150) > 150 - min(height.toInt() / 1000, 20) && real && createBonus) {
            bonus = Entity()
            bonus.add(TransformComponent(x , y + 30, 100.0f, 100.0f))
            when(Random.nextInt(3)) {
                0 -> {
                    bonus.add(DrawComponent(Assets.bonusAppleDef))
                    bonus.add(BonusComponent(BonusComponent.APPLE))
                    bonus.add(AnimationComponent(Assets.animApple))
                }
                1 -> {
                    bonus.add(DrawComponent(Assets.bonusPushDef))
                    bonus.add(BonusComponent(BonusComponent.PUSH))
                }
                2 -> {
                    bonus.add(DrawComponent(Assets.bonusArrowDef))
                    bonus.add(BonusComponent(BonusComponent.ARROW))
                    bonus.add(AnimationComponent(Assets.animArrow))
                }
            }
        }
        if(Random.nextInt(100) > 100 - height / 100) {
            val speed = height / 5000 + 5
            platform.add(MovingComponent(0.0f, y, Gdx.graphics.width - 100.0f, y, speed))
            bonus?.add(MovingComponent(0.0f, y + 30, Gdx.graphics.width - 100.0f, y + 30, speed))
        }
        engine.addEntity(platform)
        bonus?.let { engine.addEntity(it) }
    }
}