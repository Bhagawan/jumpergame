package com.beawergames.game.systems

import com.badlogic.ashley.core.*
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.beawergames.game.components.MovingComponent
import com.beawergames.game.components.PLayerComponent
import com.beawergames.game.components.TransformComponent
import com.beawergames.game.screen.GameScreen

class HeightControlSystem(priority: Int, private val gameState: GameScreen.GameState): IteratingSystem( Family.all().get(), priority), EntityListener {
    private var hero: Entity? = null
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val mM = ComponentMapper.getFor(MovingComponent::class.java)
    private var currDelta = 0.0f

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        engine?.addEntityListener(this)
        hero = engine?.getEntitiesFor(Family.all(PLayerComponent::class.java).get())?.firstOrNull()
    }

    override fun update(deltaTime: Float) {
        if(gameState.getGAmeState() != GameScreen.GAME_PAUSED) {
            hero?.let {
                val hTC = it.getComponent(TransformComponent::class.java)
                currDelta = hTC.pos.y - Gdx.graphics.height / 2
                if(currDelta > 0) {
                    it.getComponent(PLayerComponent::class.java)?.let { pC -> pC.height += currDelta.toInt() }
                    super.update(deltaTime)
                }
            }
        }
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val tC = tM.get(entity)
        val mC = mM.get(entity)
        if(mC != null) {
            mC.from.y -= currDelta
            mC.to.y -= currDelta
        }
        if( tC != null) {
            tC.pos.y -= currDelta
            if(tC.pos.y < -50 && entity != hero) engine.removeEntity(entity)
        }
    }

    override fun entityAdded(entity: Entity?) {
        entity?.let {
            if (it.getComponent(PLayerComponent::class.java) != null) hero = it
        }
    }

    override fun entityRemoved(entity: Entity?) {
        if(entity == hero) hero = null
    }
}