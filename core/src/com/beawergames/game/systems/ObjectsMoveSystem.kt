package com.beawergames.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.math.Vector2
import com.beawergames.game.components.MovingComponent
import com.beawergames.game.components.TransformComponent
import com.beawergames.game.screen.GameScreen
import kotlin.math.absoluteValue

class ObjectsMoveSystem(priority: Int, private val gameState: GameScreen.GameState): IteratingSystem(Family.all(MovingComponent::class.java).get(), priority) {
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val mM = ComponentMapper.getFor(MovingComponent::class.java)

    override fun update(deltaTime: Float) {
        if(gameState.getGAmeState() != GameScreen.GAME_PAUSED) super.update(deltaTime)
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val tC = tM.get(entity)
        val mC = mM.get(entity)
        if(tC != null && mC != null) {
            val dV = if(mC.speed > 0) Vector2(mC.to).sub(Vector2(tC.pos.x, tC.pos.y))
            else Vector2(mC.from).sub(Vector2(tC.pos.x, tC.pos.y))
            if(dV.len() > mC.speed.absoluteValue) dV.nor().scl(mC.speed.absoluteValue)
            else mC.speed *= -1
            tC.pos.add(dV.x, dV.y, 0.0f)
        }
    }
}