package com.beawergames.game.assets

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Array

class Assets {

    companion object {
        lateinit var sky: Texture
        lateinit var buttonUp: Texture
        lateinit var buttonDown: Texture
        lateinit var halfTransparent: Texture
        lateinit var greyColor: Texture
        lateinit var btnPause: Texture
        lateinit var left_arrow: Texture
        private lateinit var platforms: Texture
        private lateinit var hero_strip: Texture
        private lateinit var bonus_apple_strip: Texture
        private lateinit var bonus_push_strip: Texture
        private lateinit var bonus_arrow_strip: Texture


        lateinit var heroDefault: TextureRegion
        lateinit var heroJumping: TextureRegion
        lateinit var animHeroUp: Animation<TextureRegion>
        lateinit var animHeroDown: Animation<TextureRegion>

        lateinit var bonusAppleDef: TextureRegion
        lateinit var bonusPushDef: TextureRegion
        lateinit var bonusArrowDef: TextureRegion
        lateinit var animApple: Animation<TextureRegion>
        lateinit var animArrow: Animation<TextureRegion>
        lateinit var animPush: Animation<TextureRegion>

        lateinit var platformReal: TextureRegion
        lateinit var platformBroken: TextureRegion

        lateinit var russianFont: BitmapFont
        lateinit var defSkin: Skin

        private fun loadTexture(file: String?): Texture {
            return Texture(Gdx.files.internal(file))
        }

        fun create() {
            sky = loadTexture("sky.png")
            buttonUp = loadTexture("menu_button_default.png")
            buttonDown = loadTexture("menu_button_pressed.png")
            btnPause = loadTexture("pause_button.png")
            left_arrow = loadTexture("left_arrow.png")

            platforms = loadTexture("platforms.png")
            platformBroken = TextureRegion(platforms, 0, 16, platforms.width, 16)
            platformReal = TextureRegion(platforms, platforms.width, 16)

            hero_strip = loadTexture("hero_strip.png")
            heroDefault = TextureRegion(hero_strip, 16, hero_strip.height)
            heroJumping = TextureRegion(hero_strip,16 * 10, 4, 16, hero_strip.height)
            val heroFrames = TextureRegion.split(hero_strip, 16, 24)
            val upFrames: Array<TextureRegion> = Array(Array<TextureRegion> (9) { t -> heroFrames[0][t] })
            animHeroUp = Animation(0.025f, upFrames)
            val downFrames: Array<TextureRegion> = Array(Array<TextureRegion> (7) { t -> if(t < 6) heroFrames[0][9 + t] else heroFrames[0][0]})
            animHeroDown = Animation(0.015f, downFrames)

            bonus_apple_strip = loadTexture("Apple.png")
            bonus_push_strip = loadTexture("push.png")
            bonus_arrow_strip = loadTexture("bonus_arrow.png")
            bonusAppleDef = TextureRegion(bonus_apple_strip, 32, hero_strip.height)
            bonusPushDef = TextureRegion(bonus_push_strip, 28, hero_strip.height)
            bonusArrowDef = TextureRegion(bonus_arrow_strip, 18, hero_strip.height)

            val appleFrames = TextureRegion.split(bonus_apple_strip, 32,32)
            animApple = Animation(0.025f, Array(Array<TextureRegion> (appleFrames[0].size) { t -> appleFrames[0][t] }), Animation.PlayMode.LOOP)
            val pushFrames = TextureRegion.split(bonus_push_strip, 28,28)
            animPush = Animation(0.025f, Array(Array<TextureRegion> (pushFrames[0].size) { t -> pushFrames[0][t] }), Animation.PlayMode.LOOP)
            val arrowFrames = TextureRegion.split(bonus_arrow_strip, 18,18)
            animArrow = Animation(0.025f, Array(Array<TextureRegion> (arrowFrames[0].size) { t -> arrowFrames[0][t] }), Animation.PlayMode.LOOP)

            createColors()
            generateFont()
            createDefSkin()
        }

        private fun createDefSkin() {
            defSkin = Skin()

//            val pixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
//            pixmap.setColor(Color.WHITE)
//            pixmap.fill()
//            defSkin.add("white", Texture(pixmap))
//
//            defSkin.add("default", BitmapFont())

            val textButtonStyle = TextButton.TextButtonStyle()
            textButtonStyle.font = russianFont
            textButtonStyle.up = TextureRegionDrawable(buttonUp)
            textButtonStyle.down = TextureRegionDrawable(buttonDown)

            defSkin.add("menuButtonStyle", textButtonStyle)
        }

        private fun generateFont() {
            val parameter = FreeTypeFontParameter()
            parameter.characters = ("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
                    + "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
                    + "1234567890.,:;_¡!¿?\"'+-*/()[]={}")
            parameter.size = 35

            val generator = FreeTypeFontGenerator(Gdx.files.internal("MazzardSoft.ttf"))
            russianFont = generator.generateFont(parameter)

             generator.dispose()
        }

        private fun createColors() {
            val pixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
            pixmap.setColor(Color(1.0f,1.0f,1.0f,0.5f))
            pixmap.fill()
            halfTransparent = Texture(pixmap)
            pixmap.setColor(Color(0.35f, 0.35f, 0.35f,1.0f))
            pixmap.fill()
            greyColor = Texture(pixmap)
            greyColor.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
            greyColor.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
            pixmap.dispose()
        }

        fun dispose() {
            sky.dispose()
            buttonUp.dispose()
            buttonDown.dispose()
            halfTransparent.dispose()
            greyColor.dispose()
            btnPause.dispose()
            left_arrow.dispose()
            platforms.dispose()
            hero_strip.dispose()

            russianFont.dispose()
            defSkin.dispose()
        }
     }
}