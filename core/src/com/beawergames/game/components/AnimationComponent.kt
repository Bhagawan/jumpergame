package com.beawergames.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion

class AnimationComponent(val animation: Animation<TextureRegion>): Component {
    var state = 0.0f

}