package com.beawergames.game.components

import com.badlogic.ashley.core.Component

class PLayerComponent: Component {
    var height = 0
    var state = STATE_JUMP_MIDDLE
    var jumpMaxVelocity = 15.0f
    var currentVelocity = 0.0f

    companion object {
        const val STATE_IDLE = 0
        const val STATE_JUMP_UP = 1
        const val STATE_JUMP_MIDDLE = 2
        const val STATE_JUMP_DOWN = 3
    }
}