package com.beawergames.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math.Vector2

class MovingComponent(var from: Vector2, var to: Vector2,var speed: Float): Component {

    constructor(fromX: Float, fromY: Float, toX: Float, toY: Float, speed: Float): this(Vector2(fromX, fromY), Vector2(toX, toY), speed)

}