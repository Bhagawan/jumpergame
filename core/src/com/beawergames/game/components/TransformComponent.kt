package com.beawergames.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math.Vector3

class TransformComponent(): Component {
    val pos: Vector3 = Vector3(0f,0f,0f)
    var angle = 0.0f
    var scale = 1.0f
    var width = 1.0f
    var height = 1.0f


    constructor(x: Float, y: Float, width: Float, height: Float) : this() {
        pos.set(x, y, 0.0f)
        this.width = width
        this.height = height
    }

    constructor(x: Int, y: Int, width: Int, height: Int) : this(x.toFloat(), y.toFloat(), width.toFloat(), height.toFloat())
}