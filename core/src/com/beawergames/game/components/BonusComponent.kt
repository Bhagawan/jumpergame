package com.beawergames.game.components

import com.badlogic.ashley.core.Component

class BonusComponent(val type: Int): Component {
    companion object {
        const val APPLE = 0
        const val PUSH = 1
        const val ARROW = 2
    }
}