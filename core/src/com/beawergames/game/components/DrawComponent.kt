package com.beawergames.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.TextureRegion

class DrawComponent(var textureRegion: TextureRegion) : Component