package com.beawergames.game.screen

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.ScreenUtils
import com.beawergames.game.JumperGame
import com.beawergames.game.assets.Assets
import com.beawergames.game.util.Records

class RecordsScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createScreen()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(JumperGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createScreen() {
        val table = Table()
        val back = Table()

        back.setFillParent(true)
        back.background = TextureRegionDrawable(Assets.sky)
        back.center().add(table).width(Gdx.graphics.width - 50.0f).height(Gdx.graphics.height - 50.0f)
        stage.addActor(back)

        //table.debug = true
        table.pad(50.0f)

        val recordGroup = VerticalGroup()
        val scroll = ScrollPane(recordGroup)
        scroll.setFlickScroll(true)
        table.center().add(scroll).expand().top()
        recordGroup.space(10.0f)

        val records = Records.getRecords()
        records.sortDescending()
        val platform = TextureRegionDrawable(Assets.platformReal)
        val slime = TextureRegionDrawable(Assets.heroDefault)
        for(r in records.indices) {
            val record = Label("  ${records[r]}  ", Label.LabelStyle(Assets.russianFont, Color.WHITE) )
            val slimeImage = Table()
            slimeImage.center().add(Image(when(r) {
                0 -> slime.tint(Color.GOLD)
                1 -> slime.tint(Color.GRAY)
                else -> slime
            })).center().bottom().width(100.0f).height(100.0f)
            slimeImage.row()
            slimeImage.add(Image(platform)).center().top().width(100.0f).height(30.0f)


            val recordItem = Table()
            recordItem.setSize(300.0f, 75.0f)


            if(r % 2 == 0) {
                record.setAlignment(Align.right)
                recordItem.add(record).center().right().expand().width(200.0f)
                recordItem.add(slimeImage).center().expand()
                recordItem.add().width(200.0f)
            } else {
                record.setAlignment(Align.left)
                recordItem.add().expand().width(200.0f)
                recordItem.add(slimeImage).center().expand()
                recordItem.add(record).center().left().expand().width(200.0f)
            }
            recordGroup.addActor(recordItem)


//            val cont = Container(record)
//            record.setAlignment(Align.center)
//            cont.size(300.0f, 75.0f)
//            when(r) {
//                0 -> {
//                    cont.background = recordBack
//                    record.setFontScale(1.2f)
//                    record.color = Color.RED
//                }
//                1 -> cont.background = recordBack.tint(Color.GRAY)
//                2 -> cont.background = recordBack.tint(Color.BROWN)
//                else -> cont.background = recordBack.tint(Color.BLACK)
//            }
//            recordGroup.addActor(cont)
        }

        val backButtonBackUP = TextureRegionDrawable(Assets.buttonDown).tint(Color.WHITE)
        val backButtonBackDown = TextureRegionDrawable(Assets.buttonDown).tint(Color.GRAY)
        val backButton = ImageButton(
            ImageButton.ImageButtonStyle(backButtonBackUP, backButtonBackDown, null, TextureRegionDrawable(Assets.left_arrow), null, null))

        backButton.addListener(object : ClickListener() {
            override fun touchUp(
                event: InputEvent?,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ) {
                game.screen = MenuScreen(game)
            }
        })
        table.row()
        table.add(backButton).bottom().center().height(50.0f)
    }
}