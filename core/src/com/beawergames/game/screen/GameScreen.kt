package com.beawergames.game.screen

import com.badlogic.ashley.core.Engine
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.ScreenUtils
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.beawergames.game.assets.Assets
import com.beawergames.game.systems.*

class GameScreen(private val game: Game): ScreenAdapter() {
    private val batch = SpriteBatch()
    private val camera = OrthographicCamera()
    private val engine = Engine()

    private var gameState = GAME_ON
    private val stateListener = object : GameState {
        override fun getGAmeState(): Int {
            return gameState
        }
    }

    private val controlSystem = HeroControlSystem(7, stateListener)
    private val uiSystem = UISystem(11)

    companion object {
        const val GAME_ON = 0
        const val  GAME_PAUSED = 1
    }

    init {
        val viewport = ScreenViewport(camera)
        viewport.update(Gdx.graphics.width, Gdx.graphics.height, true)
        camera.update()
        batch.projectionMatrix = camera.combined

        val loseSystem = LoseCheckSystem(1)
        loseSystem.setInterface(object : LoseCheckSystem.LoseInterface {
            override fun onLost(height: Int) {
                uiSystem.showEndScreen(height)
            }
        })
        uiSystem.setInterface(object : UISystem.UIInterface {
            override fun pause(pause: Boolean) = when(pause) {
                true -> gameState = GAME_PAUSED
                else -> gameState = GAME_ON
            }

            override fun exit() {
                game.screen = MenuScreen(game)
            }

            override fun newGame() {
                engine.removeAllEntities()
                engine.removeSystem(engine.getSystem(SpawnSystem::class.java))
                engine.addSystem(SpawnSystem(1))
            }
        })

        engine.addSystem(SpawnSystem(1))
        engine.addSystem(BonusInteractionSystem(5))
        engine.addSystem(ObjectsMoveSystem(3, stateListener))
        engine.addSystem(loseSystem)
        engine.addSystem(controlSystem)
        engine.addSystem(HeightControlSystem(6, stateListener))
        engine.addSystem(RenderSystem(batch, 10))
        engine.addSystem(uiSystem)
    }

    override fun show() {
         val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = MenuScreen(game)
                    true
                }
                else -> false
            }

            override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                if(screenX > Gdx.graphics.width / 2) controlSystem.move(false) else controlSystem.move(true)
                return true
            }

            override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                controlSystem.stopMove()
                return true
            }

            override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
                if(screenX > Gdx.graphics.width / 2) controlSystem.move(false) else controlSystem.move(true)
                return true
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(uiSystem.getStage())
        inputMultiplexer.addProcessor(inputAdapter)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0f, 0f, 0f, 1f)
        batch.projectionMatrix = camera.combined
        camera.update()

        batch.begin()
        batch.draw(Assets.sky, 0f,0f, camera.viewportWidth, camera.viewportHeight)
        batch.end()


        engine.update(delta)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun dispose() {
        batch.dispose()
    }

    interface GameState {
        fun getGAmeState(): Int
    }
}