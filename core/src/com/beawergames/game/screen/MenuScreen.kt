package com.beawergames.game.screen

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.ScreenUtils
import com.beawergames.game.JumperGame
import com.beawergames.game.assets.Assets


class MenuScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(JumperGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val table = Table()
        //table.debug = true
        table.setFillParent(true)
        table.background = TextureRegionDrawable(Assets.sky)
        stage.addActor(table)

        val width = (Gdx.graphics.width / 2.5).toFloat()

        val newGameButton = TextButton("Новая Игра", Assets.defSkin.get("menuButtonStyle", TextButton.TextButtonStyle::class.java))
        newGameButton.addListener(object : ClickListener() {
            override fun touchUp(
                event: InputEvent?,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ) {
                game.screen = GameScreen(game)
            }
        })
        table.add(newGameButton).size(width, 100f).expandY().bottom()
        table.row()

        val recordsButton = TextButton("Рекорды", Assets.defSkin.get("menuButtonStyle", TextButton.TextButtonStyle::class.java))
        recordsButton.addListener(object : ClickListener() {
            override fun touchUp(
                event: InputEvent?,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ) {
                game.screen = RecordsScreen(game)
            }
        })
        table.add(recordsButton).size(width, 100f).bottom().padBottom(100.0f).padTop(100.0f)
        table.row()

        val exitButton = TextButton("Выход", Assets.defSkin.get("menuButtonStyle", TextButton.TextButtonStyle::class.java))
        exitButton.addListener( object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                Gdx.app.exit()
            }
        })
        table.add(exitButton).size(width, 100f).expandY().bottom().padBottom(50.0f)
        table.row()

    }
}